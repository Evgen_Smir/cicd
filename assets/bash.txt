#!/bin/bash

THRESHOLD=20
EMAIL=smrnovz@mail.ru

while true; do
    COUNT=$(grep 'bitrix/admin' /var/log/httpd/access_log | grep -c $(date +%Y:%m:%d))
    if [[ $COUNT -gt $THRESHOLD ]]; then
        echo "Пороговое значение обращений превышено. Количество обращений: $COUNT" | mail -s "Уведомление о превышении порога" -r VlaJeka@yandex.ru $EMAIL
    fi
    sleep 300
done
